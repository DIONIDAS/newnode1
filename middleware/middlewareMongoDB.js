const ENV = require("dotenv").config({})
const MongoClient = require("mongodb").MongoClient
class MongoCLass {
   #url = process.env.MONGO_URL
   constructor() {
       return new Promise(async (resolve, reject) => {
           try {
               let connection = await MongoClient.connect(this.#url)
               return resolve(connection)
           } catch (err) {
               return reject(err)
           }
       })
   }
}
module.exports = MongoCLass
