const express = require('express')
const router = express.Router()
let middlewareCors = require('./middlewareCORS')
let middleBodyParser = require('./middlewareBodyParser')
let middlewareMongoDB = require('./middlewareMongoDB')
router.use('/', middlewareCORS)
router.use('/', middlewareBodyParser)
router.use('/', middlewareMongoDB)
module.express = router